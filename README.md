# dbTools

## Background

When I started a new role as Quantitative Analyst at Thomson Reuters (then Thomson Financial, and now part of Nasdaq), I found myself making many manual edits to peer analysis and other valuation-related files. There was *a lot* of room for human error. So, on my own time, I created simple macros to speed up these edits and minimize the chance for error. Over time, my needs became more complex so I began learning VBA to tackle bigger issues. As my list of macros grew, I added them to a toolbar. 

This worked fine until my team upgraded to Excel 2007 - I then needed to learn how to create a 'ribbon'. And when I learned that others on my team and throughout my office had a use for these macros and functions, I packaged them in a [Nullsoft](https://nsis.sourceforge.io/Main_Page) installer and made it available to all.

I don't really have a need for them any longer, but I just discovered that it still works in Excel 2010 so I thought I'd share it on Git. Why not?

## Installation

Either use the installer, which simply extracts the file or follow the instructions below.

Copy dbTools.xlam to C:\Users\USERNAME\AppData\Roaming\Microsoft\AddIns,
where **USERNAME** is *your* user id in windows.

Then launch Excel, choose File > Options > Add-ins. At the bottom of the 
window, you should see the word, "Manage:" and "Excel Add-ins" in a neighboring
drop-down menu. Press the "Go" button and enable dbTools in the new window. 

You should then find a new dbTools menu item that includes a number of quick search
macros as well as value and text modifying macros. You will also find a handful of 
special functions in the functions dialog -- click the "insert function" or "fx" 
button then select the dbTools category.
